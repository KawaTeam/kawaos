KawaOS
======

A fun little x86 operating system for experimenting.
Made by Kawa Team.

## License

This project is licenced under the GNU General Public License v3.0.

## Dependencies

* GCC
* Nasm
* GRUB
* make
* qemu-system-i386
* xorriso
* mtools

## Compilation

```
make
```

## Test with QEMU

```
make launch
```

# Acknowledgments

Thanks to https://whiteheadsoftware.dev/operating-systems-development-for-dummies/ for the inspiration !