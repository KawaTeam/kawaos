#ifndef KAWA_GDT_H__
#define KAWA_GDT_H__

#include "../types.h"

// Global Descriptor Table structures and functions
// Initialized during the boot by the kernel init function

// Represents an entry in the GDT
// Granularity :
/// Description of granularity byte format :
/// - Granularity (0 -> 1 byte ; 1 -> 1 kbyte)
/// - Operand size (0 -> 16bit ; 1 -> 32bit)
/// - Zero
/// - Zero
/// 4 remaining bits : segment length
struct gdt_entry_struct_t
{
    uint16_t limit_low;       // The lower 16 bits of the limit
    uint16_t base_low;        // The lower 16 bits of the base
    uint8_t  base_middle;     // The next 8 bits of the base
    uint8_t  access;          // Access flags
    uint8_t  granularity;     // See above
    uint8_t  base_high;       // The last 8 bits of the base
} __attribute__((packed));

// Pointer structure describing the memory position of the GDT
struct gdt_ptr_struct_t
{
    uint16_t limit;
    uint32_t base;
} __attribute__((packed));

// Initializes the Global Descriptor Table (GDT)
void init_gdt();

#endif // KAWA_GDT_H__