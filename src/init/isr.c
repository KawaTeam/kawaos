#include "isr.h"
#include <string.h>

registers_handler_t interrupt_handlers[256];

void register_interrupt_handler(uint8_t n, registers_handler_t handler)
{
    interrupt_handlers[n] = handler;
}

void reset_interrupt_handlers()
{
    memset(&interrupt_handlers, 0, sizeof(registers_handler_t) * 256);
}

void irq_handler(struct registers_t regs)
{
    // Send an EOI (end of interrupt) signal to the PICs.
    // If this interrupt involved the slave.
    if (regs.int_no >= 40)
    {
        // Send reset signal to slave.
        outb(0xA0, 0x20);
    }
    // Send reset signal to master. (As well as slave, if necessary).
    outb(0x20, 0x20);

    if (interrupt_handlers[regs.int_no] != 0)
    {
        registers_handler_t handler = interrupt_handlers[regs.int_no];
        handler(regs);
    }
}
