#ifndef KAWA_ISR_H__
#define KAWA_ISR_H__

#include "../types.h"

// Represents a common IRQ interrupt handler
typedef void (*registers_handler_t)(struct registers_t);

#define IRQ0 32
#define IRQ1 33
#define IRQ2 34
#define IRQ3 35
#define IRQ4 36
#define IRQ5 37
#define IRQ6 38
#define IRQ7 39
#define IRQ8 40
#define IRQ9 41
#define IRQ10 42
#define IRQ11 43
#define IRQ12 44
#define IRQ13 45
#define IRQ14 46
#define IRQ15 47

// Registers an interrupt handler
void register_interrupt_handler(uint8_t n, registers_handler_t handler);

// Sets to 0 all interrupt handlers
void reset_interrupt_handlers();

#endif // KAWA_ISR_H__