#ifndef KAWA_IDT_H__
#define KAWA_IDT_H__

#include "../types.h"

// Interrupt Descriptor Table structures and functions
// Initialized during the boot by the kernel init function

// Describes an interrupt gate
// Flags :
/// Description of flags byte format :
/// - Presence bit
/// - 2 bits : Privilege level
/// 5 remaining bits : 0b0110 (14)
struct idt_entry_struct_t
{
    uint16_t base_lo;         // The lower 16 bits of the address to jump to when this interrupt fires
    uint16_t selector;        // Kernel segment selector
    uint8_t  zero;            // Zero
    uint8_t  flags;           // Flags, see above
    uint16_t base_hi;         // The upper 16 bits of the address to jump to
} __attribute__((packed));

// Describes a pointer to an array of interrupt handlers
struct idt_ptr_struct_t
{
    uint16_t limit;
    uint32_t base;
} __attribute__((packed));

// Initialize the Interrupt Descriptor Table (IDT)
void init_idt();

#endif // KAWA_IDT_H__