#include "gdt.h"
#include "idt.h"
#include "isr.h"

// Called by boot.asm
void kernel_init()
{
    // Setting up the Global Descriptor Table
    init_gdt();

    // Setting up the Interrupt Descriptor Table
    init_idt();

    // Setting to NULL all interrupt handlers
    reset_interrupt_handlers();
}
