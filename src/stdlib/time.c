#include "time.h"
#include "../timer.h"

time_t time(time_t* timer)
{
    // NOTE: THIS IS NOT A REAL TIME VALUE
    // IT IS USE FOR SRAND TEST PURPOSE
    const time_t timestamp = _rdtsc_eax();
    if (timer != NULL)
        *timer = timestamp;
    return timestamp;
}
