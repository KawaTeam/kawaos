#ifndef KAWA_TIME_H__
#define KAWA_TIME_H__

#include "../types.h"

#define time_t unsigned int

// RETURNS THE NUMBER OF CYCLES OF CPU FROM START NOT THE ACTUAL TIME
time_t time(time_t* timer);

#endif // KAWA_TIME_H__