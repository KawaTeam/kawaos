#include "ctype.h"

// Character classification functions

// Check if character is alphanumeric
int isalnum(int c)
{
    if ((c >= 48 && c <= 57) || (c >= 65 && c <= 90) || (c >= 97 && c <= 122)) 
        return 0;
    return 1;
}

// Check if character is alphabetic
int isalpha(int c)
{
    if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122))
        return 0;
    return 1;
}

// Check if character is a control character
int iscntrl(int c)
{
    return isalnum(c) - 1;
}

// Check if character is decimal digit
int isdigit(int c)
{
    if (c >= 48 && c <= 57)
        return 0;
    return 1;
}

// Check if character has graphical representation
int isgraph(int c)
{
    if (c >= 32 && c <= 126)
        return 0;
    return 1;
}

// Check if character is lowercase letter
int islower(int c)
{
    if (c >= 97 && c <= 122)
        return 0;
    return 1;
}

// Check if character is printable
int isprint(int c)
{
    if (c >= 31 && c <= 126)
        return 0;
    return 1;
}

// Check if character is a punctuation character
int ispunct(int c)
{
    if ((c >= 33 && c <= 47) || (c >= 58 && c <= 64) ||( c >= 91 && c <= 96) || (c >= 123 && c <= 126))
        return 0;
    return 1;
}

// Check if character is a white-space
int isspace(int c)
{
    if ((c >= 9 && c <= 13) || c == 32)
        return 0;
    return 1;
}

// Check if character is uppercase letter
int isupper(int c)
{
    if (c >= 65 && c <= 90)
        return 0;
    return 1;
}

// Check if character is hexadecimal digit
int isxdigit(int c)
{
    if ((c >= 48 && c <= 57) || (c >= 65 && c <= 70) || (c >= 97 && c <= 102))
        return 0;
    return 1;
}

// Character conversion functions

// Convert uppercase letter to lowercase
int tolower(int c)
{
    if (isupper(c))
        return c + 32;
    return c;
}

// Convert lowercase letter to uppercase
int toupper(int c)
{
    if (islower(c))
        return c - 32;
    return c;
}
