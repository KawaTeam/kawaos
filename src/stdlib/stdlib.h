#ifndef KAWA_STDLIB_H__
#define KAWA_STDLIB_H__

#include "../types.h"

// Success termination code
#define EXIT_SUCCESS 0

// Failure termination code
#define EXIT_FAILURE 1

// Maximum value returned by rand
#define RAND_MAX 32767

// Pseudo-random sequence generation

// Initialize random number generator
void srand(unsigned int seed);

// Generate random number
int rand();

// Integer arithmetics

struct div_t
{
    int quot; // quotient
    int rem; // remainder
};

// Absolute value
int abs(int c);

// Integral division
struct div_t div(int numer, int denom);

#endif // KAWA_STDLIB_H__