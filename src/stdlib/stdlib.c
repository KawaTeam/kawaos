#include "stdlib.h"

// Pseudo-random sequence generation

unsigned long next = 1;

// Initialize random number generator
void srand(unsigned int seed)
{
    next = seed;
}

// Generate random number
int rand()
{
    next = next * 1103515245 + 12345;
    return (unsigned int)(next / 65536) % RAND_MAX + 1; 
}

// Integer arithmetics

// Absolute value
int abs(int c)
{
    if (c < 0)
        return c * -1;
    return c;
}

// Integral division
struct div_t div(int numer, int denom)
{
    struct div_t result;
    result.quot = -1;
    result.rem = -1;

    if (denom == 0)
        return result;

    // TO DO
    // There is a better way to calculate at the same time the quotient and the remainder

    result.quot = numer / denom;
    result.rem = numer % denom;

    return result;
}
