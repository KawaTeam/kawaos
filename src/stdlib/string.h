#ifndef KAWA_CSTRING_H__
#define KAWA_CSTRING_H__

#include "../types.h"

// Copying

// Copy block of memory
void* memcpy(void* destination, const void* source, size_t num);

// Move block of memory
void* memmove(void* destination, const void* source, size_t num);

// Copy string
char* strcpy(char* destination, const char* source);

// Copy characters from string
char* strncpy(char* destination, const char* source, size_t num);

// Concatenation

// Concatenate strings
char* strcat(char* destination, const char* source);

// Append characters from string
char* strncat(char* destination, const char* source, size_t num);

// Searching

// Locate first occurrence of character in string
const char* strchr(const char* str, int character);

// Locate characters in string
const char* strpbrk (const char* str1, const char* str2);

// Locate last occurrence of character in string
const char* strrchr(const char* str, int character);

// Other

// Fill block of memory
void* memset(void* ptr, int value, size_t num);

// Returns the length of the given string
size_t strlen(const char* str);

#endif // KAWA_CSTRING_H__