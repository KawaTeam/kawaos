#include "string.h"

// Copying

// Copy block of memory
void* memcpy(void* destination, const void* source, size_t num)
{
    if (source != NULL && destination != NULL && num > 0)
    {
        for (size_t i = 0u ; i < num ; ++i)
            *((char*)(destination + i)) = *((char*)(source + i));
    }

    return destination;
}


// Move block of memory
void* memmove(void* destination, const void* source, size_t num)
{
    return memcpy(destination, source, num);
}

// Copy string
char* strcpy(char* destination, const char* source)
{
    if (source != NULL && destination != NULL)
    {
        for ( ; *source != 0 ; ++source)
            *(destination++) = *source;
    }
    
    return destination;
}

// Copy characters from string
char* strncpy(char* destination, const char* source, size_t num)
{
    if (source != NULL && destination != NULL)
    {
        for ( ; num != 0 ; --num)
        {
            if (*source != 0)
                *(destination++) = *(source++);
            else
                *(destination++) = 0;
        }
    }

    return destination;
}

// Concatenation

// Concatenate strings
char* strcat(char* destination, const char* source)
{
    if (source != NULL && destination != NULL)
    {
        while (*(destination++) != 0);

        strcpy(destination, source);
    }

    return destination;
}

// Append characters from string
char* strncat(char* destination, const char* source, size_t num)
{
    if (source != NULL && destination != NULL)
    {
        while (*(destination++) != 0);

        for ( ; *source != 0 && num > 0 ; --num)
            *(destination++) = *(source++);
    }

    return destination;
}

// Searching

// Locate first occurrence of character in string
const char* strchr(const char* str, int character)
{
    const char* str_end = str + strlen(str);
    const char* ptr = str;

    for ( ; *ptr != character && ptr < str_end ; ++ptr);

    if (ptr == str_end)
        return NULL;
    return ptr;
}

// Locate characters in string
const char* strpbrk (const char* str1, const char* str2)
{
    const char* str_end = str1 + strlen(str1);

    for (const char* ptr = str1 ; ptr < str_end ; ++ptr)
    {
        if (strchr(str2, *ptr) != NULL)
            return ptr;
    }

    return NULL;
}

// Locate last occurrence of character in string
const char* strrchr(const char* str, int character)
{
    const char* ptr = str + strlen(str);

    for ( ; *ptr != character && ptr >= str ; --ptr);

    if (ptr < str)
        return NULL;
    return ptr;
}

// Other

// Fill block of memory
void* memset(void* ptr, int value, size_t num)
{
    if (ptr != NULL)
    {
        unsigned char* i_ptr = (unsigned char*) ptr;
        for (size_t i_num = 0 ; i_num < num ; ++i_num)
            *i_ptr = (unsigned char) value;
    }

    return ptr;
}

// Returns the length of the given string
size_t strlen(const char* str)
{
    size_t len = 0;
    while (str[len])
        len++;
    return len;
}
