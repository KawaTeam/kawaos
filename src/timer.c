#include <time.h>
#include "types.h"
#include "timer.h"
#include "init/isr.h"

void delay(const int t)
{   
    volatile int i, j;
    for (i = 0 ; i < t ; ++i)
        for (j = 0 ; j < 250000 ; ++j)
            __asm__("NOP");
}

uint64_t rdtsc()
{
	return (uint64_t)_rdtsc_eax() | (uint64_t)_rdtsc_edx() << 32;
}

uint32_t pit_tick = 0;
uint32_t pit_frequency = 0;

static void timer_callback(struct registers_t regs)
{
    ++pit_tick;
}

void init_timer(uint32_t frequency)
{
    // Firstly, register our timer callback
    register_interrupt_handler(IRQ0, &timer_callback);

    // The value we send to the PIT is the value to divide it's input clock
    // (1193180 Hz) by, to get our required frequency. Important to note is
    // that the divisor must be small enough to fit into 16-bits.
    uint32_t divisor = 1193180 / frequency;
    pit_frequency = frequency;

    // Send the command byte
    outb(0x43, 0x36);

    // Divisor has to be sent byte-wise, so split here into upper/lower bytes
    uint8_t l = (uint8_t)(divisor & 0xFF);
    uint8_t h = (uint8_t)((divisor>>8) & 0xFF);

    // Send the frequency divisor
    outb(0x40, l);
    outb(0x40, h);
}

uint32_t get_pic_tick()
{
    return pit_tick;
}
