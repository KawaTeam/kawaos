#ifndef KAWA_TYPES_H__
#define KAWA_TYPES_H__

#define size_t unsigned short

#define uint8_t unsigned char
#define uint16_t unsigned short
#define uint32_t unsigned int
#define uint64_t unsigned long long

#define sint8_t char
#define sint16_t short
#define sint32_t int
#define sint64_t long long

#define NULL (void*)0

// Write a byte out to the specified port
void outb(uint16_t port, uint8_t value);
uint8_t inb(uint16_t port);
uint16_t inw(uint16_t port);

// Describes the register values
struct registers_t
{
   uint32_t ds;
   uint32_t edi;
   uint32_t esi;
   uint32_t ebp;
   uint32_t esp;
   uint32_t ebx;
   uint32_t edx;
   uint32_t ecx;
   uint32_t eax;
   uint32_t int_no;
   uint32_t err_code;
   uint32_t eip;
   uint32_t cs;
   uint32_t eflags;
   uint32_t useresp;
   uint32_t ss;
};

#endif // KAWA_TYPES_H__