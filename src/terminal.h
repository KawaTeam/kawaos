#ifndef KAWA_TERMINAL_H__
#define KAWA_TERMINAL_H__

#include "types.h"

#define VGA_WIDTH  160
#define VGA_HEIGHT 25

enum vga_colour
{
    VGA_COLOUR_BLACK,
    VGA_COLOUR_BLUE,
    VGA_COLOUR_GREEN,
    VGA_COLOUR_CYAN,
    VGA_COLOUR_RED,
    VGA_COLOUR_MAGENTA,
    VGA_COLOUR_BROWN,
    VGA_COLOUR_LIGHT_GREY,
    VGA_COLOUR_DARK_GREY,
    VGA_COLOUR_LIGHT_BLUE,
    VGA_COLOUR_LIGHT_GREEN,
    VGA_COLOUR_LIGHT_CYAN,
    VGA_COLOUR_LIGHT_RED,
    VGA_COLOUR_LIGHT_MAGENTA,
    VGA_COLOUR_LIGHT_BROWN,
    VGA_COLOUR_WHITE
};

struct terminal_s
{
    uint16_t* buffer;

    size_t width;
    size_t height;

    size_t row;
    size_t column;
    uint8_t color;
};

void terminal_putchar(struct terminal_s* p_terminal, char c);

// Writes a char to the terminal
void terminal_putchar_at(struct terminal_s* p_terminal, char c, uint16_t color, size_t x, size_t y);

// Writes a string to the terminal
void terminal_writestring(struct terminal_s* p_terminal, const char* data);

void terminal_writestring_colour(struct terminal_s* p_terminal, const char * data, enum vga_colour fg, enum vga_colour bg);

void terminal_writeint(struct terminal_s* p_terminal, unsigned long n);

void terminal_set_row_column(struct terminal_s* p_terminal, const size_t row, const size_t column);

// Initialize screen terminal
void terminal_initialize(struct terminal_s* p_terminal);


#endif // KAWA_TERMINAL_H__