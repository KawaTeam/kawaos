#include "terminal.h"
#include <stdlib.h>
#include <time.h>
#include "timer.h"

// Called by boot.asm
void kernel_main()
{
    // Initialise timer to 60Hz
    init_timer(60);

    // Initialization of the terminal interface
    struct terminal_s p_terminal;
    terminal_initialize(&p_terminal);

    // Initialization of the pseudo-random generator
    srand(time(NULL));
    
    // Kawa presentation
    terminal_set_row_column(&p_terminal, 1, 1);
    terminal_writestring_colour(&p_terminal, "Welcome to Kawa OS\n", VGA_COLOUR_WHITE, VGA_COLOUR_LIGHT_BLUE);
    terminal_set_row_column(&p_terminal, 2, 1);
    terminal_writestring_colour(&p_terminal, "By KawaTeam\n", VGA_COLOUR_WHITE, VGA_COLOUR_LIGHT_BLUE);
    terminal_set_row_column(&p_terminal, 3, 1);
    terminal_writestring_colour(&p_terminal, "Licensed under GPL v3\n", VGA_COLOUR_WHITE, VGA_COLOUR_LIGHT_BLUE);
    
    // Main loop
    for (;;)
    {
        // Cleanning then writing a random number
        terminal_set_row_column(&p_terminal, 9, 2);
        terminal_writestring(&p_terminal, "                                      ");
        terminal_set_row_column(&p_terminal, 9, 2);
        terminal_writestring_colour(&p_terminal, "Rand():", VGA_COLOUR_BLACK, VGA_COLOUR_WHITE);
        terminal_putchar(&p_terminal, ' ');
        terminal_writeint(&p_terminal, rand());

        // Test
        terminal_set_row_column(&p_terminal, 10, 2);
        terminal_writestring(&p_terminal, "                                      ");
        terminal_set_row_column(&p_terminal, 10, 2);
        terminal_writestring_colour(&p_terminal, "Uptime after end of init:", VGA_COLOUR_BLACK, VGA_COLOUR_WHITE);
        terminal_putchar(&p_terminal, ' ');
        terminal_writeint(&p_terminal, get_pic_tick() / 60);
        terminal_writestring(&p_terminal, " s");

        delay(1000);
    }
}
