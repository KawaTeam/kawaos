#include "terminal.h"
#include <string.h>

static inline uint8_t vga_entry_color(enum vga_colour foreground, enum vga_colour background)
{
    return foreground | (background << 4);
}

static inline uint16_t vga_entry(unsigned char c, uint8_t color)
{
    return (uint16_t) c | ((uint16_t) color << 8);
}

void terminal_putchar_at(struct terminal_s* p_terminal, char c, uint16_t colour, size_t x, size_t y)
{
    const size_t index = y * VGA_WIDTH + x;
    p_terminal->buffer[index] = vga_entry(c, colour);
}

void terminal_scroll_up(struct terminal_s* p_terminal)
{
    int i;
    for(i = 0; i < (VGA_WIDTH*VGA_HEIGHT-80); i++)
        p_terminal->buffer[i] = p_terminal->buffer[i+80];
    for(i = 0; i < VGA_WIDTH; i++)
        p_terminal->buffer[(VGA_HEIGHT - 1) * VGA_WIDTH + i] = vga_entry(' ', p_terminal->color);
}

void terminal_putchar(struct terminal_s* p_terminal, char c)
{
    if (p_terminal->column == VGA_WIDTH || c == '\n')
    {
        p_terminal->column = 0;
        if (p_terminal->row == VGA_HEIGHT-1)
            terminal_scroll_up(p_terminal);
        else
            ++p_terminal->row;
    }
    if (c == '\n')
        return;
    terminal_putchar_at(p_terminal, c, p_terminal->color, p_terminal->column++, p_terminal->row);
}

void terminal_write(struct terminal_s* p_terminal, const char* data, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        terminal_putchar(p_terminal, data[i]);
}

void terminal_writestring(struct terminal_s* p_terminal, const char* data)
{
    terminal_write(p_terminal, data, strlen(data));
}

void terminal_writestring_colour(struct terminal_s* p_terminal, const char * data, enum vga_colour fg, enum vga_colour bg)
{
    uint8_t oldcolor = p_terminal->color;
    p_terminal->color = vga_entry_color(fg, bg);
    terminal_writestring(p_terminal, data);
    p_terminal->color = oldcolor;
}

void terminal_writeint(struct terminal_s* p_terminal, unsigned long n)
{
    if (n / 10)
        terminal_writeint(p_terminal, n / 10);
    terminal_putchar(p_terminal, (n % 10) + '0');
}

void terminal_set_row_column(struct terminal_s* p_terminal, const size_t row, const size_t column)
{
    p_terminal->row = row;
    p_terminal->column = column;
}

void terminal_initialize(struct terminal_s* p_terminal)
{
    p_terminal->buffer = (uint16_t *) 0xB8000;

    p_terminal->width = VGA_WIDTH;
    p_terminal->height = VGA_HEIGHT;

    p_terminal->row = 0;
    p_terminal->column = 0;
    p_terminal->color = vga_entry_color(VGA_COLOUR_WHITE, VGA_COLOUR_BLUE);

    for(size_t y = 0; y < VGA_HEIGHT; ++y)
    {
        for(size_t x = 0; x < VGA_WIDTH; ++x)
        {
            const size_t index = y * VGA_WIDTH + x;
            p_terminal->buffer[index] = vga_entry(' ', p_terminal->color);
        }
    }
}
