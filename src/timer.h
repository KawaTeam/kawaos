#ifndef KAWA_TIMER_H__
#define KAWA_TIMER_H__

// Defined in boot.asm
extern time_t _rdtsc_edx();
extern time_t _rdtsc_eax();

void delay(const int t);

// Returns the value of the RDTSC x86 instruction
uint64_t rdtsc();

// Returns the number of ticks of the PIC timer.
uint32_t get_pic_tick();

// Initializes the timer
void init_timer(uint32_t frequency);

#endif // KAWA_TIMER_H__