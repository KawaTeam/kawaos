; RDTSC
global _rdtsc_edx
global _rdtsc_eax

_rdtsc_edx:
	rdtsc
	mov eax, edx
	ret
_rdtsc_eax:
	rdtsc
	ret