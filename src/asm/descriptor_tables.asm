extern isr_handler
extern isr_common_handler
extern irq_handler

global _gdt_flush
global _idt_flush

; Loads a new GDT
; Parameter : pointer to the new GDT
_gdt_flush:
   mov eax, [esp+4]  ; GDT pointer
   lgdt [eax]        ; Load the new GDT pointer

   mov ax, 0x10      ; 0x10 is the offset in the GDT to our data segment
   mov ds, ax        ; Load all data segment selectors
   mov es, ax
   mov fs, ax
   mov gs, ax
   mov ss, ax
   jmp 0x08:.flush   ; 0x08 is the offset to our code segment
.flush:
   ret

; Loads a new IDT
; Parameter : pointer to the new IDT
_idt_flush:
   mov eax, [esp+4]  ; Get the pointer to the IDT, passed as a parameter.
   lidt [eax]        ; Load the IDT pointer.
   ret

; Common interrupt handler
; When a specific interrupt is received, this function is called
; A zero and the value of the interrupt are pushed on the stack
isr_common_handler:
   pusha

   mov ax, ds        ; Lower 16-bits of eax = ds
   push eax          ; Save the data segment descriptor

   mov ax, 0x10      ; Load the kernel data segment descriptor
   mov ds, ax
   mov es, ax
   mov fs, ax
   mov gs, ax

   call isr_handler  ; Calling the kernel interrupt handler

   pop eax           ; Reload the original data segment descriptor
   mov ds, ax
   mov es, ax
   mov fs, ax
   mov gs, ax

   popa
   add esp, 8        ; Cleans up the pushed zero and pushed ISR number
   sti
   iret

; IRQ stub
; It saves the processor state, sets up for kernel mode segments, calls the C-level fault handler and finally restores the stack frame.
irq_common_stub:
   pusha

   mov ax, ds         ; Lower 16-bits of eax = ds
   push eax           ; Save the data segment descriptor

   mov ax, 0x10       ; Load the kernel data segment descriptor
   mov ds, ax
   mov es, ax
   mov fs, ax
   mov gs, ax

   call irq_handler   ; Calling the kernel IRQ handler

   pop ebx            ; Reload the original data segment descriptor
   mov ds, bx
   mov es, bx
   mov fs, bx
   mov gs, bx

   popa
   add esp, 8         ; Cleans up the pushed zero and pushed ISR number
   sti
   iret

; Interrupt handlers
global isr0
global isr1
global isr2
global isr3
global isr4
global isr5
global isr6
global isr7
global isr8
global isr9
global isr10
global isr11
global isr12
global isr13
global isr14
global isr15
global isr16
global isr17
global isr18
global isr19
global isr20
global isr21
global isr22
global isr23
global isr24
global isr25
global isr26
global isr27
global isr28
global isr29
global isr30
global isr31
; int 0x0 handler
; Division by zero
isr0:
  cli
  push byte 0
  push byte 0
  jmp isr_common_handler
; int 0x1 handler
; Debug exception
isr1:
  cli
  push byte 0
  push byte 1
  jmp isr_common_handler
; int 0x2 handler
; Non maskable interrupt
isr2:
  cli
  push byte 0
  push byte 2
  jmp isr_common_handler
; int 0x3 handler
; Breakpoint exception
isr3:
  cli
  push byte 0
  push byte 3
  jmp isr_common_handler
; int 0x4 handler
; Into detected overflow
isr4:
  cli
  push byte 0
  push byte 4
  jmp isr_common_handler
; int 0x5 handler
; Out of bounds exception
isr5:
  cli
  push byte 0
  push byte 5
  jmp isr_common_handler
; int 0x6 handler
; Invalid opcode exception
isr6:
  cli
  push byte 0
  push byte 6
  jmp isr_common_handler
; int 0x7 handler
; No coprocessor exception
isr7:
  cli
  push byte 0
  push byte 7
  jmp isr_common_handler
; int 0x8 handler
; Double fault
isr8:
  cli
  push byte 0
  push byte 8
  jmp isr_common_handler
; int 0x9 handler
; Coprocessor segment overrun
isr9:
  cli
  push byte 0
  push byte 9
  jmp isr_common_handler
; int 0xA handler
; Bad TSS
isr10:
  cli
  push byte 0
  push byte 10
  jmp isr_common_handler
; int 0xB handler
; Segment not present
isr11:
  cli
  push byte 0
  push byte 11
  jmp isr_common_handler
; int 0xC handler
; Stack fault
isr12:
  cli
  push byte 0
  push byte 12
  jmp isr_common_handler
; int 0xD handler
; General protection fault
isr13:
  cli
  push byte 0
  push byte 13
  jmp isr_common_handler
; int 0xE handler
; Page fault
isr14:
  cli
  push byte 0
  push byte 14
  jmp isr_common_handler
; int 0xF handler
; Unknown interrupt exception
isr15:
  cli
  push byte 0
  push byte 15
  jmp isr_common_handler
; int 0x10 handler
; Coprocessor fault
isr16:
  cli
  push byte 0
  push byte 16
  jmp isr_common_handler
; int 0x11 handler
; 
isr17:
  cli
  push byte 0
  push byte 17
  jmp isr_common_handler
; int 0x12 handler
; Machine check exception
isr18:
  cli
  push byte 0
  push byte 18
  jmp isr_common_handler
; 0x13 -> 0x1F : Reserved
; int 0x13 handler
isr19:
  cli
  push byte 0
  push byte 19
  jmp isr_common_handler
; int 0x14 handler
isr20:
  cli
  push byte 0
  push byte 20
  jmp isr_common_handler
; int 0x15 handler
isr21:
  cli
  push byte 0
  push byte 21
  jmp isr_common_handler
; int 0x16 handler
isr22:
  cli
  push byte 0
  push byte 22
  jmp isr_common_handler
; int 0x17 handler
isr23:
  cli
  push byte 0
  push byte 23
  jmp isr_common_handler
; int 0x18 handler
isr24:
  cli
  push byte 0
  push byte 24
  jmp isr_common_handler
; int 0x19 handler
isr25:
  cli
  push byte 0
  push byte 25
  jmp isr_common_handler
; int 0x1A handler
isr26:
  cli
  push byte 0
  push byte 26
  jmp isr_common_handler
; int 0x1B handler
isr27:
  cli
  push byte 0
  push byte 27
  jmp isr_common_handler
; int 0x1C handler
isr28:
  cli
  push byte 0
  push byte 28
  jmp isr_common_handler
; int 0x1D handler
isr29:
  cli
  push byte 0
  push byte 29
  jmp isr_common_handler
; int 0x1E handler
isr30:
  cli
  push byte 0
  push byte 30
  jmp isr_common_handler
; int 0x1F handler
isr31:
  cli
  push byte 0
  push byte 31
  jmp isr_common_handler

; IRQ
global irq0
global irq1
global irq2
global irq3
global irq4
global irq5
global irq6
global irq7
global irq8
global irq9
global irq10
global irq11
global irq12
global irq13
global irq14
global irq15
irq0:
  cli
  push byte 0
  push byte 32
  jmp irq_common_stub
irq1:
  cli
  push byte 0
  push byte 33
  jmp irq_common_stub
irq2:
  cli
  push byte 0
  push byte 34
  jmp irq_common_stub
irq3:
  cli
  push byte 0
  push byte 35
  jmp irq_common_stub
irq4:
  cli
  push byte 0
  push byte 36
  jmp irq_common_stub
irq5:
  cli
  push byte 0
  push byte 37
  jmp irq_common_stub
irq6:
  cli
  push byte 0
  push byte 38
  jmp irq_common_stub
irq7:
  cli
  push byte 0
  push byte 39
  jmp irq_common_stub
irq8:
  cli
  push byte 0
  push byte 40
  jmp irq_common_stub
irq9:
  cli
  push byte 0
  push byte 41
  jmp irq_common_stub
irq10:
  cli
  push byte 0
  push byte 42
  jmp irq_common_stub
irq11:
  cli
  push byte 0
  push byte 43
  jmp irq_common_stub
irq12:
  cli
  push byte 0
  push byte 44
  jmp irq_common_stub
irq13:
  cli
  push byte 0
  push byte 45
  jmp irq_common_stub
irq14:
  cli
  push byte 0
  push byte 46
  jmp irq_common_stub
irq15:
  cli
  push byte 0
  push byte 47
  jmp irq_common_stub