; Declaration of constants
MBALIGN  equ  1
MEMINFO  equ  2
FLAGS    equ  MBALIGN | MEMINFO
MAGIC    equ  0x1BADB002
CHECKSUM equ -(MAGIC + FLAGS)

; Setting the multiboot
; Magic number, flags and checksum
section .multiboot
align 4
	dd MAGIC
	dd FLAGS
	dd CHECKSUM

; Construction of a small stack
; x86 standard 16 byte aligned stack
; 16 KiB reserved for the stack
;section .bss
;align 16
;stack_bottom:
;resb 16384
;stack_top:

; Main code
section .text
global _start:function (_start.end - _start)

; Kernel entry point
extern kernel_main
extern kernel_init
_start:
	;mov esp, stack_top
	push ebx
	cli					; Disable interrupts
	call kernel_init
	sti					; Enable interrupts
	call kernel_main
	jmp $				; Prevents from executing RAM values beyond kernel code
.end: