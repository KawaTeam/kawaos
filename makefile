all:
	$(MAKE) -C src

launch:
	$(MAKE) launch -C src

clean:
	$(MAKE) clean -C src